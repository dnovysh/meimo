﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meimo.Person.AdditionalInfo.DataAccess.Data
{
    public class AdditionalInfoDatabaseSettings : IAdditionalInfoDatabaseSettings
    {
        public string AdditionalInfoCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IAdditionalInfoDatabaseSettings
    {
        string AdditionalInfoCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}

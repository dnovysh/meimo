﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Meimo.Person.AdditionalInfo.Core.Domain
{
    public class BaseEntity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }
        public BaseEntity()
        {
            Id = ObjectId.GenerateNewId();
        }
    }

    public class EducationEntity
    {
        public string College { get; set; }
        public int GraduationYear { get; set; }
    }
}

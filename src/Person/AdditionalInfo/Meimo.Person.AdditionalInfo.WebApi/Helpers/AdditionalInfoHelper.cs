﻿using Meimo.Person.AdditionalInfo.Core.Domain;
using Meimo.Person.AdditionalInfo.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Meimo.Person.AdditionalInfo.WebApi.Helpers
{
    public static class AdditionalInfoHelper
    {
        public static AdditionalInfoEntity MapRequestToEntity(this CreatePersonAdditionalInfoRequest request)
        {
            return new AdditionalInfoEntity()
            {
                PersonId = request.PersonId,
                About = request.About,
                Sports = request.Sports,
                Films = request.Films,
                Games = request.Games,
                Movies = request.Movies,
                Musics = request.Musics,
                Educations = request.Educations.Select(x => new EducationEntity() { College = x.College, GraduationYear = x.GraduationYear }).ToList()
            };
        }
        public static PersonAdditionalInfoResponse MapEntityToResponse(this AdditionalInfoEntity entity)
        {
            if (entity == null) return new PersonAdditionalInfoResponse();

            return new PersonAdditionalInfoResponse()
            {
                PersonId = entity.PersonId,
                About = entity.About,
                Sports = entity.Sports.Select(x => x).ToList(),
                Films = entity.Films.Select(x => x).ToList(),
                Games = entity.Games.Select(x => x).ToList(),
                Movies = entity.Movies.Select(x => x).ToList(),
                Musics = entity.Musics.Select(x => x).ToList(),
                Educations = entity.Educations.Select(x => new EducationResponse() { College = x.College, GraduationYear = x.GraduationYear }).ToList()
            };
        }
    }
}

﻿using System;

namespace Meimo.Person.AdditionalInfo.WebApi.Models
{
    public class EducationResponse
    {
        public string College { get; set; }
        public int GraduationYear { get; set; }
    }
    public class EducationRequest
    {
        public string College { get; set; }
        public int GraduationYear { get; set; }
    }
}

﻿using System;

using Grpc.Core;

namespace Meimo.Person.Profile.GrpcServer
{
    public sealed partial class ProfilePictureChangeRequestModel
    {
        public static void Validate(ProfilePictureChangeRequestModel model)
        {
            if (!Guid.TryParse(model.Id, out _))
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Profile Id must be a GUID"));
            }
        }
    }
}

﻿using System;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace Meimo.Person.Account.Core.Helpers
{
    public class ActivationTokenHelper
    {
        public static string NewToken()
        {
            var uniqueString = Guid.NewGuid().ToString();

            // generate a 128-bit salt using a secure PRNG
            byte[] salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            // derive a 256-bit subkey
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: uniqueString,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 100,
                numBytesRequested: 256 / 8));

            string result = Regex.Replace(hashed, @"[^\d\w]", string.Empty);

            return result;
        }
    }
}

﻿using System;

using Meimo.Person.Account.Core.Entities;

namespace Meimo.Person.Account.Core.Dtos
{
    public class ProfileDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Gender Gender { get; set; }

        public string GenderName { get; set; }

        public bool ShowGender { get; set; }

        public DateTimeOffset? Birthday { get; set; }

        public bool ShowBirthday { get; set; }

        public int CountryId { get; set; }

        public Country Country { get; set; }

        public string City { get; set; }
    }
}

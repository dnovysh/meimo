﻿namespace Meimo.Person.Account.EmailNotifierClient
{
    public class SendEmailRequest
    {
        /// <summary>
        /// От кого
        /// </summary>
        public string From { get; set; }

        /// <summary>
        /// Копия
        /// </summary>
        public string Cc { get; set; }

        /// <summary>
        /// Кому
        /// </summary>
        public string To { get; set; }

        /// <summary>
        /// Тема письма
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Текст письма
        /// </summary>
        public string Body { get; set; }
    }
}

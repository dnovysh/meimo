﻿using System;

using Meimo.Person.Account.Core.Dtos;

namespace Meimo.Person.Account.WebApi.Models
{
    public class LoginResponse
    {
        public string Phone { get; set; }

        public string Email { get; set; }

        public LoginResponse(LoginDto loginDto)
        {
            Phone = loginDto.Phone;
            Email = loginDto.Email;
        }
    }
}

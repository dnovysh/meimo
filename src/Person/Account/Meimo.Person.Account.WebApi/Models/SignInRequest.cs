﻿using System.ComponentModel.DataAnnotations;

namespace Meimo.Person.Account.WebApi.Models
{
    public class SignInRequest
    {
        [Required]
        [MaxLength(100), MinLength(5)]
        public string Login { get; set; }

        [Required]
        [MaxLength(30), MinLength(5)]
        public string Password { get; set; }
    }
}

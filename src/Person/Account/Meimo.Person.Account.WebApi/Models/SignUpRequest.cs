﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Meimo.Person.Account.Core.Entities;
using Meimo.Person.Account.Core.Interfaces;
using Meimo.Person.Account.WebApi.Helpers;

namespace Meimo.Person.Account.WebApi.Models
{
    public class SignUpRequest : IValidatableObject
    {
        [MaxLength(30), MinLength(5)]
        [Phone]
        public string PhoneNumber { get; set; }

        [MaxLength(100), MinLength(5)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [MaxLength(30), MinLength(5)]
        public string Password { get; set; }

        [Required]
        [MaxLength(30), MinLength(5)]
        public string PasswordConfirmation { get; set; }

        [Required]
        [MaxLength(100), MinLength(1)]
        public string FirstName { get; set; }

        [MaxLength(200)]
        public string LastName { get; set; }

        [Required]
        public short GenderId { get; set; }

        public DateTimeOffset? Birthday { get; set; }

        [Required]
        public int CountryId { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrWhiteSpace(PhoneNumber) && string.IsNullOrWhiteSpace(Email))
            {
                yield return new ValidationResult(
                    "Не заданы ни телефон ни адрес электронной почты.",
                    new[] { nameof(PhoneNumber), nameof(Email) });

            }

            var passwordValidationResult = PasswordHelper.ValidatePasswordConfirmation(
                Password,
                PasswordConfirmation,
                nameof(Password),
                nameof(PasswordConfirmation));

            if (passwordValidationResult != ValidationResult.Success)
            {
                yield return passwordValidationResult;
            }
        }

        public PersonAccount MapToPersonAccount(IDateTimeOffsetNow dateTimeOffsetNow, IGuidNew guidNew)
        {
            var activated = !string.IsNullOrWhiteSpace(PhoneNumber);

            var personAccount = new PersonAccount
            {
                Id = guidNew.Value,
                Phone = PhoneNumber,
                Email = Email,
                Password = Password,
                PasswordDate = dateTimeOffsetNow.Value,
                FirstName = FirstName,
                LastName = LastName,
                Gender = (Gender)GenderId,
                ShowGender = false,
                Birthday = Birthday,
                ShowBirthday = false,
                CountryId = CountryId,
                SysLanguage = SysLanguage.Russian,
                Activated = activated,
                Locked = false,
                Deleted = false
            };

            return personAccount;
        }
    }
}

﻿using Meimo.Person.Account.Core.Entities;

namespace Meimo.Person.Account.WebApi.Models
{
    public class CountryResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string InvariantName { get; set; }

        public CountryResponse(Country country)
        {
            Id = country.Id;
            Name = country.Name;
            InvariantName = country.InvariantName;
        }
    }
}

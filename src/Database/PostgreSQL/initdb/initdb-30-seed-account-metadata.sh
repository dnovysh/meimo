#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "meimo" <<-EOSQL
BEGIN;
INSERT INTO person."Countries" ("Id", "Name", "InvariantName", "SortOrder") VALUES ('643'::integer, 'Россия'::text, 'Russia'::text, '10'::integer) returning "Id";
INSERT INTO person."Countries" ("Name", "InvariantName", "SortOrder", "Id") VALUES ('Беларусь'::text, 'Belarus'::text, '1000'::integer, '112'::integer) returning "Id";
INSERT INTO person."Countries" ("Id", "Name", "InvariantName", "SortOrder") VALUES ('398'::integer, 'Казахстан'::text, 'Kazakhstan'::text, '1000'::integer) returning "Id";
COMMIT;
EOSQL
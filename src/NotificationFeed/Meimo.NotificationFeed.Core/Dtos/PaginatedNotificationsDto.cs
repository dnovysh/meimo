﻿using System.Collections.Generic;

namespace Meimo.NotificationFeed.Core
{
    public class PaginatedNotificationsDto
    {
        public List<Notification> Notifications { get; set; }

        public string NextPageToken { get; set; }

        public PaginatedNotificationsDto(List<Notification> notifications, string nextPageToken)
        {
            Notifications = notifications;
            NextPageToken = nextPageToken;
        }
    }
}

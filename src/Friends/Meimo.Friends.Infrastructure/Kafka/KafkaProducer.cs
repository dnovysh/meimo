using System;
using Confluent.Kafka;
using Meimo.Friends.Kafka.Contracts;
using Microsoft.Extensions.Configuration;

namespace Meimo.Friends.Infrastructure.Kafka
{
    public class KafkaProducer : IDisposable
    {
        private readonly IProducer<Null, NotificationEvent> _producer;
        private readonly string _topic;

        public KafkaProducer(IConfiguration config)
        {
            var producerConfig = new ProducerConfig();
            config.GetSection("Kafka:ProducerSettings").Bind(producerConfig);
            producerConfig.EnableIdempotence = true;
            producerConfig.Acks = Acks.All;
            producerConfig.CompressionType = CompressionType.Gzip;
            producerConfig.CompressionLevel = 9;
            _producer = new ProducerBuilder<Null, NotificationEvent>(producerConfig)
                .SetValueSerializer(new NotificationEventSerializer())
                .Build();
            
            _topic = config.GetValue<string>("Kafka:TrackingEventTopic");
        }

        public void Produce(NotificationEvent notificationEvent)
        {
            _producer.Produce(_topic, 
                new Message<Null, NotificationEvent> { Value = notificationEvent }, 
                DeliveryResultHandler.Handler);
        }

        public void Dispose()
        {
            _producer.Flush();
            _producer.Dispose();
        }
    }
}
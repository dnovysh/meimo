using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Types;
using Meimo.Friends.Core;

namespace Meimo.Friends.GraphQL.TypeExtensions
{
    [ExtendObjectType(Constants.MutationTypeName)]
    public class PersonMutations
    {
        public async Task<bool> SetPersonProfileInfo(Person input,
            [Service] IPersonRepository repository)
        {
            if (string.IsNullOrWhiteSpace(input.FirstName))
            {
                return false;
            }
            
            return await repository.SetPersonProfileInfo(input);
        }
    }
}

using System;
using Meimo.Friends.Core;

namespace Meimo.Friends.ProfileReplication.Contract
{
    public class ProfileMiddleInfo
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Avatar { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public bool Locked { get; set; }

        public bool Deleted { get; set; }

        public Person ToPerson()
        {
            return new Person
            {
                Id = Id,
                FirstName = FirstName,
                LastName = LastName,
                Avatar = Avatar,
                City = City,
                Country = Country,
                Locked = Locked,
                Deleted = Deleted
            };
        }
    }
}
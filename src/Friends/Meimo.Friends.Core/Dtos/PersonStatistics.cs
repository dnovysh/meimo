using System;

namespace Meimo.Friends.Core
{
    public class PersonStatistics
    {

        public Guid Id { get; set; }
        
        public int Friends { get; set; }
        
        public int IncomingRequests { get; set; }
        
        public int OutgoingRequests { get; set; }
        
        public int IncomingSubscriptions { get; set; }
        
        public int OutgoingSubscriptions { get; set; }
        
        public int IncomingLocks { get; set; }
        
        public int OutgoingLocks { get; set; }
    }
}

namespace Meimo.Friends.Core
{
    public enum SortingField
    {
        None = 0,
        FirstName = 1,
        LastName = 2,
        City = 3,
        Id = 4
    }
}

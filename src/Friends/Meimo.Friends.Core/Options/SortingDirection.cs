namespace Meimo.Friends.Core
{
    public enum SortingDirection
    {
        Asc = 0,
        Desc = 1
    }
}

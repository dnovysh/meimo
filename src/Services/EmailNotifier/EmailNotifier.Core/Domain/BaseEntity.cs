﻿using System;

namespace Meimo.Services.EmailNotifier.Core.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
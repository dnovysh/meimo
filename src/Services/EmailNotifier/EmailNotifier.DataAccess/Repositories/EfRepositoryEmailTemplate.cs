﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.Internal;
using Meimo.Services.EmailNotifier.Core.Abstractions.Repositories;
using Meimo.Services.EmailNotifier.Core.Domain.Notification;
using Meimo.Services.EmailNotifier.DataAccess.Data;
using Meimo.Services.EmailNotifier.DataAccess.Helpers;
using Microsoft.EntityFrameworkCore;

namespace Meimo.Services.EmailNotifier.DataAccess.Repositories
{
    public class EfEmailTemplateRepository: IRepository<EmailTemplate>
    {
        private readonly EmailDataContext _context;
        private readonly IMapper _mapper; 
        public EfEmailTemplateRepository(EmailDataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<EmailTemplate>> GetAllAsync()
        {
            return await _context.Set<EmailTemplate>().ToListAsync();
        }

        public async Task<EmailTemplate> GetByIdAsync(Guid id)
        {
            return await _context.Set<EmailTemplate>()
                .Include(p => p.TemplateFields)
                .FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<EmailTemplate> AddAsync(EmailTemplate newEntity)
        {
            await _context.Set<EmailTemplate>().AddAsync(newEntity);
            await Commit();
            return newEntity;
        }

        public async Task<EmailTemplate> UpdateAsync(EmailTemplate updatedEntity)
        {
            var emailTemplates = await _context.Set<EmailTemplate>()
                .FirstOrDefaultAsync(p=>p.Id == updatedEntity.Id);
           
            if (emailTemplates != null)
            {
                _mapper.Map<EmailTemplate, EmailTemplate>(updatedEntity, emailTemplates);
                // emailTemplates.TemplateFields = updatedEntity.TemplateFields;
                _context.Entry(emailTemplates).State = EntityState.Modified;
                                
                List<EmailTemplateField> fields = (await _context.Set<EmailTemplateField>().Where(p=>p.Template == emailTemplates).ToListAsync());
                 var deletingFields = fields.Except(updatedEntity.TemplateFields, new EmailTemplateFieldComparer()).ToList();
                 if(deletingFields.Any())
                     _context.Set<EmailTemplateField>().RemoveRange(deletingFields);
                 var existEntities = updatedEntity.TemplateFields.Except(emailTemplates.TemplateFields).ToList();

                 if (existEntities.Any())
                 {
                     existEntities.ForAll(p=>p.Template =emailTemplates);
                     await _context.Set<EmailTemplateField>().AddRangeAsync(existEntities);
                 }

                 await Commit();
            }
            return updatedEntity;
        }

        public async void DeleteAsync(Guid id)
        {
            var entity = await _context.Set<EmailTemplate>().FindAsync(id);
            if (entity == null)
                return;

            _context.Set<EmailTemplate>().Remove(entity);
            await Commit();
        }
        
        private async Task Commit()
        {
            await _context.SaveChangesAsync();
        }
    }
}
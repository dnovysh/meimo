
Сервис EmailNotifier.

Для запуска сервиса без docker compose необходимо вручную запустить:

PosgreSQL:
docker run -d --name my_postgres -v my-dbdata:/var/lib/postgresql/data -e POSTGRES_PASSWORD=pgpassword -p 54320:5432 postgres:11
RabbitMQ:
docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management


Для построения и запуска с использование docker-compose необходимо ввести:
docker compose build
docker compose up

Предварительно также необходимо прописать настройки в .env файле для возможности отправки писем.

﻿using System;
using System.Collections.Generic;

namespace Meimo.Services.EmailNotifier.Api.Models.Api
{
    /// <summary>
    /// Структура ответа шаблона сообщения
    /// </summary>
    public class EmailTemplateResponse
    {   
        /// <summary>
        /// Идентификатор шаблона сообщения
        /// </summary>
        public Guid Id { get; set; }
        
        /// <summary>
        /// Название шаблона сообщения
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Текст шаблона-сообщения
        /// </summary>
        public string TemplateText { get; set; }
        
        /// <summary>
        /// Набор полей-меток шаблона сообщения
        /// </summary>
        public List<EmailTemplateFieldResponse> TemplateFields { get; set; }
    }
}
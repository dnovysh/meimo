﻿using System;
using System.Collections.Generic;

namespace Meimo.Services.EmailNotifier.Api.Models.Api
{
    public class SendEmailTemplateRequest
    {
        /// <summary>
        /// От кого
        /// </summary>
        public string From { get; set; }
        
        /// <summary>
        /// Копия
        /// </summary>
        public string Cc { get; set; }
        
        /// <summary>
        /// Кому
        /// </summary>
        public string To { get; set; }
        
        /// <summary>
        /// Тема сообщения
        /// </summary>
        public string Subject { get; set; }
        
        /// <summary>
        /// Ссылка на шаблон
        /// </summary>
        public Guid EmailTemplateId { get; set; }
        
        /// <summary>
        /// Параметры шаблона со значениями
        /// </summary>
        public List<SendingEmailTemplateParameterRequest> EmailTemplateParameters { get; set; }        
    }
}
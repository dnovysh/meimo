﻿using System;
using System.Collections.Generic;

namespace Meimo.Services.EmailNotifier.Api.Models.Api
{
    /// <summary>
    /// Структура запроса для обновления шаблона сообщения 
    /// </summary>
    public class UpdateEmailTemplateRequest
    {
        /// <summary>
        /// Идентификатор шаблона
        /// </summary>
        public Guid Id { get; set; }
        
        /// <summary>
        /// Название шаблона
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Текст сообщения с полями-метками для замены
        /// </summary>
        public string TemplateText { get; set;}
        
        /// <summary>
        /// Список полей-меток для замены
        /// </summary>
        public List<EmailTemplateFieldRequest> TemplateFields { get; set; }
    }
}
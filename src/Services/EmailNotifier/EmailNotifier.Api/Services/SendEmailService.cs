﻿using System.Threading.Tasks;
using AutoMapper;
using Meimo.Services.EmailNotifier.Api.Infrastructure.Exceptions;
using Meimo.Services.EmailNotifier.Api.Models.Service;
using Meimo.Services.EmailNotifier.Core.Abstractions.Repositories;
using Meimo.Services.EmailNotifier.Core.Domain.Notification;
using Meimo.Services.EmailNotifier.Core.Infrastructure.Logging;
using Meimo.Services.EmailNotifier.Infrastructure.Contracts;
using Meimo.Services.EmailNotifier.Infrastructure.Queues;

namespace Meimo.Services.EmailNotifier.Api.Services
{
    public class SendEmailService:ISendEmailService
    {
        private readonly IMapper _mapper;
        private readonly ISendEmailQueue _sendEmailQueue;
        private readonly IRepository<EmailTemplate> _repositoryEmailTemplate;
        private readonly ITraceLogger _traceLogger;
        
        public SendEmailService(ISendEmailQueue sendEmailQueue, IMapper mapper, IRepository<EmailTemplate> repositoryEmailTemplate, ITraceLogger traceLogger)
        {
            _sendEmailQueue = sendEmailQueue;
            _mapper = mapper;
            _repositoryEmailTemplate = repositoryEmailTemplate;
            _traceLogger = traceLogger;
        }
        public Task SendEmailAsync(TracedObject<SendingEmail> sendingEmail)
        {
            _traceLogger.TraceLog(string.Format("{0}.{1}",nameof(SendEmailService), nameof(SendEmailAsync)), sendingEmail);
            _sendEmailQueue.AddSendEmailMessageToQueue(_mapper.Map<TracedObject<RmqSendEmailMessage>>(sendingEmail));
            return Task.CompletedTask;
        }

        public async Task SendEmailTemplateAsync(TracedObject<SendingEmailTemplate> sendingEmailTemplate)
        {
            var emailTemplate = await _repositoryEmailTemplate.GetByIdAsync(sendingEmailTemplate.Data.EmailTemplateId);
            if (emailTemplate != null)
            {
                var sendingEmail = _mapper.Map<TracedObject<SendingEmailTemplate>, TracedObject<SendingEmail>>(sendingEmailTemplate);
                sendingEmail.Data.Body= new EmailTemplateBodyBuilder(emailTemplate, sendingEmailTemplate.Data.EmailTemplateParameters).Build();
                await SendEmailAsync(sendingEmail);
            }
            else
            {
                throw new EmailNotifierException($"Не найден шаблон по идентификатору: '{sendingEmailTemplate.Data.EmailTemplateId}'");
            }
        }
    }
}
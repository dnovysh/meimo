﻿namespace Meimo.Services.EmailNotifier.Infrastructure
{
    public class RabbitMqConfiguration
    {
        public string Hostname { get; set; }

        public string QueueName { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}
﻿using System;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using Meimo.Services.EmailNotifier.Core.Infrastructure.Logging;
using Meimo.Services.Infrastructure.Smtp.Model;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;

namespace Meimo.Services.Infrastructure.Smtp
{
    public class EmailSender: IEmailSender
    {
        private readonly IOptions<SmtpClientConfiguration> _smtpClientConfiguration;
        private readonly ITraceLogger _traceLogger;
        private readonly ILogger<EmailSender> _logger;
        
        public EmailSender(IOptions<SmtpClientConfiguration> smtpClientConfiguration, ITraceLogger traceLogger, ILogger<EmailSender> logger)
        {
            _smtpClientConfiguration = smtpClientConfiguration;
            _traceLogger = traceLogger;
            _logger = logger;
        }
        
        public async Task SendEmailAsync(TracedObject<SmtpSendEmailMessage> tracedSmtpSendEmailMessage)
        {
            var emailMessage = new MimeMessage();
            
            emailMessage.From.Add(new MailboxAddress(tracedSmtpSendEmailMessage.Data.From, tracedSmtpSendEmailMessage.Data.From));
            emailMessage.To.Add(new MailboxAddress(tracedSmtpSendEmailMessage.Data.To, tracedSmtpSendEmailMessage.Data.To));
            emailMessage.Subject = tracedSmtpSendEmailMessage.Data.Subject;
            emailMessage.Body = new TextPart(TextFormat.Html) { Text = tracedSmtpSendEmailMessage.Data.Body };
            
            await Task.Run(() =>
            {
                using (var client = new SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (mysender, certificate, chain, sslPolicyErrors) => { return true; };
                    client.CheckCertificateRevocation = false;
                    
                    client.Connect(_smtpClientConfiguration.Value.Host, _smtpClientConfiguration.Value.Port, true);
                    
                    // Note: since we don't have an OAuth2 token, disable
                    // the XOAUTH2 authentication mechanism.
                    client.AuthenticationMechanisms.Remove("XOAUTH2");

                    try
                    {
                        // Note: only needed if the SMTP server requires authentication
                        client.Authenticate(_smtpClientConfiguration.Value.Username, _smtpClientConfiguration.Value.Password);

                        client.Send(emailMessage);
                        client.Disconnect(true);
                    }
                    catch (Exception exc)
                    {
                        _logger.LogCritical($"Couldn't send message for reason: {exc.Message}");
                        _traceLogger.TraceLog(tracedSmtpSendEmailMessage.TraceId, 
                            "Send message over smtp",
                            $"Couldn't send message for reason: {exc.Message}");
                    }
                }
            });
        }
    }
}